package com.example.practica1eventoboton;

import androidx.appcompat.app.AppCompatActivity;

import android.view.*;
import android.widget.*;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Del método PULSAME 2:

        // Configuramos la ventana cargando las vistas necesarias:
        cargarVistas();

        botonCalcular.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        calcular();
                    }
                }
        );
    }

    private void cargarVistas() {
        botonCalcular = (Button)findViewById(R.id.BtnCalcular);
        input = (EditText)findViewById(R.id.inputKmMillas);
        resultado = (TextView) findViewById(R.id.Resultado);

        conversiones = (RadioGroup)findViewById(R.id.opcionesDeConversion);
        aKms = (RadioButton) findViewById(R.id.opcion_a_kms);
        aMillas = (RadioButton) findViewById(R.id.opcion_a_millas);

        //CONFIGURAMOS RADIOBUTTONS: marcamos uno por defecto
        //conversiones.clearCheck(); --> Desmarca todas las opciones
        conversiones.check(R.id.opcion_a_kms); //Marca una opción determinada mediante su ID
    }

    private void calcular() {
        // Usar Toast para lanzar un mensaje de aviso si el EditText esta vacío al pulsar el botón
        // método getText().length()

        if(input.getText().length()==0){
            resultado.setText("");

            // NOTIFICACIÓN DE ERROR:
            Toast inputVacio = new Toast(getApplicationContext());
            LayoutInflater inflater = getLayoutInflater();

            //Linkamos con el nombre del xml del layout del Toast
            View layoutToast = inflater.inflate(R.layout.toast_layout,
                    //Linkamos con el id del layout principal del xml del Toast :
                    (ViewGroup) findViewById(R.id.toast_inputVacio));

            /* En caso de personalizar tambien el mensaje..
            En este caso el mensaje es estático y lo incluyo en el xml linkado a strings.xml
            TextView mensaje = (TextView)layout.findViewById(R.id.txtMensaje);
            mensaje.setText("Toast Personalizado");
            */

            inputVacio.setDuration(Toast.LENGTH_SHORT);
            inputVacio.setView(layoutToast);
            inputVacio.show();
        }else{
            double kmMill = Double.valueOf(String.valueOf(input.getText()));
            //getCheckedRadioButtonId() devolverá el ID de la opción marcada -1 si no hay ninguna marcada:
                //int idSeleccionado = conversiones.getCheckedRadioButtonId();

            if (conversiones.getCheckedRadioButtonId()==R.id.opcion_a_kms) {
                resultado.setText("Millas son " + String.valueOf(String.format("%.2f", kmMill / 0.6214)) + " Kms");
            } else {
                resultado.setText("Kms son " + String.valueOf(String.format("%.2f", kmMill * 0.6214)) + " Millas");
            }
        }
    }
    private Button botonCalcular;
    private EditText input;
    private TextView resultado;
    private RadioGroup conversiones;
    private RadioButton aKms;
    private RadioButton aMillas;
}
